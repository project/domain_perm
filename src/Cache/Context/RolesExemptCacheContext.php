<?php

declare(strict_types=1);

namespace Drupal\domain_perm\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\Core\Site\Settings;

/**
 * A context for the roles exempt.
 *
 * Cache context ID: 'roles_exempt'.
 */
final class RolesExemptCacheContext implements CacheContextInterface {

  public function __construct(
    protected Settings $settings,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel(): string {
    return (string) t('Roles exempt');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext(): string {
    $exempt = $this->settings->get('domain_perm_roles_exempt', ['anonymous', 'authenticated']);
    return implode(',', $exempt);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata(): CacheableMetadata {
    return new CacheableMetadata();
  }

}
