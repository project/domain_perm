<?php

namespace Drupal\domain_perm\AccessPolicy;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccessPolicyBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\CalculatedPermissionsItem;
use Drupal\Core\Session\RefinableCalculatedPermissionsInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\HttpFoundation\RequestStack;

class UserRolesAccessPolicy extends AccessPolicyBase {

  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected Settings $settings,
    protected RequestStack $request_stack,
  ) {}

  public function calculatePermissions(AccountInterface $account, string $scope): RefinableCalculatedPermissionsInterface {
    $calculated_permissions = parent::calculatePermissions($account, $scope);
    $effective_roles = $this->effectiveRoles($account);
    foreach ($effective_roles as $user_role) {
      $calculated_permissions
        ->addItem(new CalculatedPermissionsItem($user_role->getPermissions(), $user_role->isAdmin()))
        ->addCacheableDependency($user_role);
    }

    return $calculated_permissions;
  }

  /**
   * Reduce role list when not on edit domain.
   *
   * @return \Drupal\user\RoleInterface[]
   *   Roles that are effective for the current domain.
   */
  public function effectiveRoles(AccountInterface $account): array {
    /** @var \Drupal\user\RoleInterface[] $all_roles */
    $all_roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple($account->getRoles());

    if ($this->isEditDomain()) {
      return $all_roles;
    }

    // Use the Setting below to identify roles that are allowed on non-edit domain.
    $roles_exempt = $this->settings->get('domain_perm_roles_exempt', ['anonymous', 'authenticated']);
    return array_intersect_key($all_roles, array_flip($roles_exempt));
  }

  /**
   * Check if the current domain is an edit domain.
   */
  public function isEditDomain(): bool {
    return str_contains($this->request_stack->getCurrentRequest()->getHost(), '-content');
  }

  /**
   * Ensure that the policy varies by domain.
   */
  public function getPersistentCacheContexts(): array {
    return ['user.roles', 'url.site', 'roles_exempt'];
  }

}
