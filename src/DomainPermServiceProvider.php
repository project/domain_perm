<?php

declare(strict_types=1);

namespace Drupal\domain_perm;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;
use Drupal\domain_perm\AccessPolicy\UserRolesAccessPolicy;
use Symfony\Component\DependencyInjection\Reference;

final class DomainPermServiceProvider implements ServiceModifierInterface {

  public function alter(ContainerBuilder $container): void {

    if ($container->hasDefinition('access_policy.user_roles')) {
      $definition = $container->getDefinition('access_policy.user_roles');
      $definition->setClass(UserRolesAccessPolicy::class);
      $definition->setArguments([
        new Reference('entity_type.manager'),
        new Reference('settings'),
        new Reference('request_stack'),
      ]);
    }
  }

}
